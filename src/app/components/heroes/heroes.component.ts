import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Hero } from '../../classes/Hero';
import { HeroService } from '../../services/hero.service';

@Component({
  selector: 'heroes',
  templateUrl: './heroes.component.html' ,
  styleUrls: ['./heroes.component.css'],
  providers: [HeroService]
})

export class HeroesComponent implements OnInit {
  name = 'Tour of Heroes';
  heroes: Hero[];
  selectedHero: Hero;

  constructor(private heroService: HeroService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.getHeroes();
  }

  add(heroName: string): void{
    heroName = heroName.trim();
    if(!heroName)return;
    this.heroService.addHero(heroName)
      .then(hero=>{this.heroes.push(hero);
        this.selectedHero = null});
  }

  delete(hero: Hero): void {
  this.heroService
      .delete(hero.id)
      .then(() => {
        this.heroes = this.heroes.filter(h => h !== hero);
        if (this.selectedHero === hero) { this.selectedHero = null; }
      });
}


  getHeroes(): void {
    this.heroService.getHeroesSlowly().then(heroes => this.heroes = heroes);
  }

  onSelect(hero: Hero): void {
    this.selectedHero = hero;
  }

  goToDetail(): void {
    this.router.navigate(['/detail', this.selectedHero.id]);
  }
}
