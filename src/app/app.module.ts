import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule}    from '@angular/forms';
import { HttpModule}     from '@angular/http';

import { AppRoutingModule} from './app-routing.module';


import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService }  from './services/in-memory-data.service';


import { AppComponent }  from './app.component';
import { HeroesComponent} from './components/heroes/heroes.component';
import { HeroDetailComponent } from './components/hero-detail/hero-detail.component';
import { HeroService } from './services/hero.service';
import { DashboardComponent} from './components/dashboard/dashboard.component';
import { HeroSearchComponent} from './components/hero-search/hero-search.component';

@NgModule({
  imports:      [ BrowserModule,
                  FormsModule,
                  HttpModule,
                  InMemoryWebApiModule.forRoot(InMemoryDataService),
                  AppRoutingModule
                ],
  declarations: [ AppComponent,
                  HeroesComponent,
                  HeroDetailComponent,
                  DashboardComponent, 
                  HeroSearchComponent],
  providers:    [ HeroService ],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }